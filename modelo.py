# __author__ = "Ana Isabel Jerez Uzcategui"
"""Sistema para el control de ventas de una heladeria"""

import pickle

class Modelo:

    @staticmethod
    def guardar_clientes(clientes):
        """Metodo para cargar una lista de clientes en un archivo Pickle"""

        pickle_out = open("clientes.pickle","wb")
        pickle.dump(clientes, pickle_out)
        pickle_out.close()

    @staticmethod
    def obtener_clientes():
        """Metodo obtener de un archivo Pickle la lista de clientes"""

        pickle_in = open("clientes.pickle","rb")
        clientes = pickle.load(pickle_in)
        pickle_in.close()
        return clientes

    @staticmethod
    def guardar_ordenes(ordenes):
        """Metodo para cargar una lista de clientes en un archivo Pickle"""

        pickle_out = open("ordenes.pickle","wb")
        pickle.dump(ordenes, pickle_out)
        pickle_out.close()

    @staticmethod
    def obtener_ordenes():
        """Metodo obtener de un archivo Pickle la lista de clientes"""

        pickle_in = open("ordenes.pickle","rb")
        ordenes = pickle.load(pickle_in)
        pickle_in.close()
        return ordenes