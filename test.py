#!/usr/bin/python3
from vista import *
from controlador import *

# __author__: "Ana Isabel Jerez Uzcategui"

"""SISTEMA DE CONTROL DE VENTAS DE UNA HELADERIA"""


class Aplicacion:
    """Clase destinada a la ejecucion de la aplicacion"""

    @staticmethod
    def main():
        """Punto de inicio del programa"""
        # Se instancian los objetos de los diferentes productos

        Controlador.iniciar()
        Menu.menu_principal()


class Menu:
    """Clase que contiene el menu principal"""

    @staticmethod
    def menu_principal():
        """Metodo para el menu principal"""
        Vista.menu_principal()


if __name__ == '__main__':
    Aplicacion.main()
