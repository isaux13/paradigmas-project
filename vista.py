# __author__ = "Ana Isabel Jerez Uzcategui"
"""Sistema para el control de ventas de una heladeria"""

from os import *
import sys
import time

from colorama import Back, Cursor, Fore, Style

from controlador import *

class Vista:
    """Clase Vista para los métodos que ayudarán 
        a interactuar con los usuarios
    """
 
    # -------- Metodos para el formato de impresion --------
    @staticmethod
    def limpiar_pantalla():
        """Método para limpiar la pantalla"""

        system('clear')

    @staticmethod
    def imprimir_titulo(texto):
        """
        Metodo que imprime un texto con fondo rojo y centrado
            @params
                texto: texto a imprimir
        """
        
        texto = '{:^35}'.format(texto)
        Vista.imprimir_delimitador()
        print(Fore.WHITE, Back.RED, texto, Style.BRIGHT, Style.RESET_ALL)

    @staticmethod
    def imprimir_centrado_verde(texto):
        """
        Metodo que imprime un texto verde y centrado
            @params
                texto: texto a imprimir
                *obj: un objeto adicional, se imprime debajo del texto
        """

        texto = '{:^35}'.format(texto)
        print(Fore.GREEN, '\n', texto, Style.RESET_ALL)

    @staticmethod
    def imprimir_centrado_rojo(texto):
        """
        Metodo que imprime un texto rojo y centrado
            @params:
                texto: texto a imprimir
        """

        texto = '{:^35}'.format(texto)
        print(Fore.RED, texto, Style.RESET_ALL)

    @staticmethod
    def imprimir_delimitador():
        """Metodo que imprime un delimitador"""

        texto = '--------------------------------------'
        print(Fore.RED, texto, Style.RESET_ALL)

    @staticmethod
    def imprimir_subtitulo_verde(texto):
        """Metodo que imprime un texto con fondo verde y centrado"""

        texto = '{:^20}'.format(texto)
        print(Back.GREEN, Fore.BLACK, texto, Style.RESET_ALL, '\n')

    @staticmethod
    def imprimir_texto_rojo(texto):
        """Metodo que imprime un texto rojo"""

        texto = '{:^20}'.format(texto)
        print(Fore.RED, texto, Style.RESET_ALL)

    @staticmethod
    def mostrar_barra_menus(principal=False, productos=False, clientes=False):

        menu_pri = '{:^17}'.format('MENU PRINCIPAL')
        menu_pro = '{:^17}'.format('MENU PRODUCTOS')
        menu_cli = '{:^17}'.format('MENU CLIENTES')
        Vista.limpiar_pantalla()
        if principal:
            print(Fore.WHITE, Back.RED, menu_pri, Fore.BLACK, Back.MAGENTA, menu_pro, Back.MAGENTA, menu_cli, Style.RESET_ALL)
        elif productos:
            print(Fore.BLACK, Back.MAGENTA, menu_pri, Fore.WHITE, Back.RED, menu_pro, Back.MAGENTA, Fore.BLACK, menu_cli, Style.RESET_ALL)
        else:
            print(Fore.BLACK, Back.MAGENTA, menu_pri, Back.MAGENTA, Fore.BLACK, menu_pro, Back.RED, Fore.WHITE, menu_cli, Style.RESET_ALL)

        print(Fore.RED, '---------------------------------------------------------', Style.RESET_ALL)

    @staticmethod
    def imprimir_diccionario(dicc):
        """Método que imprime el primer elemeneto de un 
            diccionaro de la forma dic = {key: value} 
            donde value son tuplas (element, ...)
                @parametros:
                    dicc: diccionario que se imprimirá
        """

        Vista.imprimir_delimitador()
        for clave,valor in dicc.items():
            print('[' , Back.RED, clave, Style.RESET_ALL, ']', valor[0])
            Vista.imprimir_delimitador()

    @staticmethod
    def convertir_lista_diccionario(lista):
        """Metodo que convierte una lista de productos en un diccionario
            @parametros:
                lista: lista que se transforma en diccionario"""

        dic = {}
        for i in lista:
            dic[str(lista.index(i)+1)] = ('     ' + i.descripcion, i) # diccionario: {key: (string, i)}
        return dic

    # -------- Metodos para el formato de lectura --------
    @staticmethod
    def pedir_enter():
        """Metodo que, al clickear enter, continua con el flujo normal"""
        
        input('\n' + Back.CYAN + Fore.BLACK + 'Enter para continuar...' + Style.RESET_ALL + '\n')

    @staticmethod
    def leer_opcion_menu(menu):
        """Metodo para leer y validar la opcion de un 
            menu contenido en un diccionario donde el key 
            es la opcion y value el texto que se mostrará
                @parametros
                    menu: un diccionario
        """
        
        while True:
            opcion = input('\n' + Back.GREEN + Fore.BLACK + 'Ingrese una opción' + Style.RESET_ALL + '\n')
            # si la opcion es valida retorna la misma
            if opcion.upper() in menu:
                return opcion.upper()
            # de lo contrario, vuelve a pedir su ingreso
            else:
                Vista.imprimir_centrado_rojo('Ingrese una opción válida')

    @staticmethod
    def leer_numero():
        """Metodo para leer y verificar que el dato ingresado sea del tipo entero"""

        while True:
            opcion = input()
            try:
                return int(opcion)
            except ValueError:
                Vista.imprimir_texto_rojo('Ingrese un número')

    @staticmethod
    def leer_fecha():

        while True:
            fecha = input()
            try:
                datetime.strptime(fecha, '%d-%m-%Y')
                return fecha
            except ValueError:
                print('Ingrese una fecha valida')

    @staticmethod
    def leer_texto():
        # metodo para leer un texto ingresado por pantalla

        while True:
            texto = input()
            if not texto:
                print('No puede estar vacío')
            else:
                return texto

    # -------- Metodos para los menus --------
    @staticmethod
    def menu_principal():
        """Método que muestra el menú principal y lee la 
            opción del usuario
        """

        Vista.mostrar_barra_menus(principal=True)
        # el diccionario contiene la opción (key) y 
        # sus values son tuplas (texto del menu, método correspondiente)
        dic_menu = {'1': ('    Menú Productos    ', Vista.menu_productos),
                    '2': ('    Menú Clientes     ', Vista.menu_clientes),
                    '3': ('    Ver total en caja ', Vista.mostrar_total_caja),
                    '4': ('    Realizar cierre   ', Vista.realizar_cierre),
                    '5': ('    Ver cierres       ', Vista.ver_cierres)}                 
        Vista.imprimir_diccionario(dic_menu)
        opcion = Vista.leer_opcion_menu(dic_menu)
        # se llama al metodo correspondiente a la opcion seleccionada
        dic_menu[opcion][1]()

    @staticmethod
    def menu_productos():
        """Método que muestra el menú 'productos'
            y lee la opción del usuario
        """

        Vista.mostrar_barra_menus(productos=True)
        # el diccionario contiene la opción (key) y 
        # sus values son tuplas (texto del menu, método correspondiente)
        dic_productos = {'1': ('    Vender Productos     ', Vista.crear_orden),
                         '2': ('    Devolver Productos   ', Vista.menu_devolver_productos),
                         '0': ('    Volver atrás         ', Vista.menu_principal)}
        Vista.imprimir_diccionario(dic_productos)
        opcion = Vista.leer_opcion_menu(dic_productos)   
        # se llama al metodo correspondiente a la opcion seleccionada
        dic_productos[opcion][1]()

    @staticmethod
    def menu_tipos_productos():
        """Método que muestra el menú 'tipo de productos'
            y lee la opción del usuario
        """

        Vista.listar_productos_orden()
        # el diccionario contiene la opción (key) y 
        # sus values son tuplas (texto del menu, método correspondiente)
        dic_tipos_productos = {'1': ('     Helados    ', Vista.menu_helados),
                               '2': ('     Envases    ', Vista.menu_envases),
                               '3': ('     Bebidas    ', Vista.menu_bebidas),
                               '4': ('     Tortas     ', Vista.menu_tortas),
                               '0': ('  Cancelar orden ', Vista.cancelar_orden)}
        Vista.imprimir_diccionario(dic_tipos_productos)
        opcion = Vista.leer_opcion_menu(dic_tipos_productos)
        # se llama al metodo correspondiente a la opcion seleccionada
        dic_tipos_productos[opcion][1]()

    @staticmethod
    def menu_devolver_productos():
        """Método que muestra el menú de los productos 
            que se pueden devolver
        """
        
        Vista.mostrar_barra_menus(productos=True)
        lista = Controlador.productos_devolvibles()
        if lista:
            dic_devolvibles = Vista.convertir_lista_diccionario(lista)
            dic_devolvibles['0'] = ('     Atrás', Vista.menu_principal)
            Vista.mostrar_barra_menus(productos=True)
            Vista.imprimir_diccionario(dic_devolvibles)
            opcion = Vista.leer_opcion_menu(dic_devolvibles)
            # se llama al metodo encargado de devolver un producto, 
            # pasandole como parametro el objeto del producto a devolver
            mensaje = Controlador.devolver_producto(dic_devolvibles[opcion][1])
            Vista.imprimir_centrado_verde(mensaje)
        else:
            Vista.imprimir_centrado_verde('\nNo hay articulos que' + \
                                            'se puedan devolver\n')
        Vista.pedir_enter()
        Vista.menu_principal()

    @staticmethod
    def menu_helados():
        """Método que muestra el menú de helados y 
            lee la opción del usuario
        """

        dic_helados = Vista.convertir_lista_diccionario(Controlador.lista_helados())
        dic_helados['0'] = ('     Atrás', Vista.menu_tipos_productos)
        Vista.listar_productos_orden()
        Vista.imprimir_titulo('SABORES')
        Vista.imprimir_diccionario(dic_helados)
        opcion = Vista.leer_opcion_menu(dic_helados)
        envase = Vista.menu_envases(True)
        # se llama al metodo correspondiente a la opcion seleccionada 
        producto = dic_helados[opcion][1]
        mensaje = Controlador.vender_producto(producto, True, envase())
        Vista.listar_productos_orden()
        Vista.imprimir_centrado_verde(mensaje)
        Vista.boton_opcion_imprimir()

    @staticmethod
    def menu_envases(para_seleccionar=False):
        """Método que muestra el menú de envases y 
            lee la opción del usuario
                @parametros:
                    para_seleccionar, si se pasa como True 
                    significa que se retornará un envase para 
                    ser vendido junto un helado
        """

        Vista.listar_productos_orden()
        lista_envases = Controlador.lista_envases()
        dic_envases = Vista.convertir_lista_diccionario(lista_envases)
        dic_envases['0'] = ('     Atrás', Vista.menu_tipos_productos)
        Vista.imprimir_titulo('ENVASES')
        # se imprime el menu de envases
        Vista.imprimir_diccionario(dic_envases)
        opcion = Vista.leer_opcion_menu(dic_envases)
        # si no es para vender con helado
        if not para_seleccionar:
            producto = dic_envases[opcion][1]
            mensaje = Controlador.vender_producto(producto)
            Vista.listar_productos_orden()
            Vista.imprimir_centrado_verde(mensaje)
            Vista.boton_opcion_imprimir()
        else:
            return dic_envases[opcion][1]

    @staticmethod
    def menu_bebidas():
        """Método que muestra el menú de bebidas y lee la opción del usuario"""

        Vista.listar_productos_orden()
        # el diccionario contiene la opción (key) y 
        # sus values son tuplas (texto del menu, método correspondiente)
        lista = Controlador.lista_bebidas()
        dic_menu_bebidas = Vista.convertir_lista_diccionario(lista)
        dic_menu_bebidas['0'] = ('     Atrás', Vista.menu_tipos_productos)
        Vista.imprimir_titulo('BEBIDAS')
        Vista.imprimir_diccionario(dic_menu_bebidas)
        opcion = Vista.leer_opcion_menu(dic_menu_bebidas)
        # se llama al metodo correspondiente a la opcion seleccionada 
        producto = dic_menu_bebidas[opcion][1]
        mensaje = Controlador.vender_producto(producto) 
        Vista.listar_productos_orden()
        Vista.imprimir_centrado_verde(mensaje)
        Vista.boton_opcion_imprimir()

    @staticmethod
    def menu_tortas():
        """Método que muestra el menú de tortas y lee la opción del usuario"""

        Vista.listar_productos_orden()
        # el diccionario contiene la opción (key) y 
        # sus values son tuplas (texto del menu, clase correspondiente)
        lista_tortas = Controlador.lista_tortas()
        dic_menu_tortas = Vista.convertir_lista_diccionario(lista_tortas)
        dic_menu_tortas['0'] = ('     Atrás', Vista.menu_tipos_productos)
        Vista.imprimir_titulo('TORTAS')
        Vista.imprimir_diccionario(dic_menu_tortas)
        opcion = Vista.leer_opcion_menu(dic_menu_tortas)
        # se llama al metodo correspondiente a la opcion seleccionada 
        producto = dic_menu_tortas[opcion][1]
        mensaje = Controlador.vender_producto(producto)
        Vista.listar_productos_orden()
        Vista.imprimir_centrado_verde(mensaje)
        Vista.boton_opcion_imprimir()

    @staticmethod
    def menu_clientes():
        """Método que muestra el menú clientes y lee la opción del usuario"""

        # el diccionario contiene la opción (key) y 
        # sus values son tuplas (texto del menu, método correspondiente)
        dic_clientes = {'1': ('    Agregar cliente      ', Vista.agregar_cliente),
                        '2': ('    Buscar cliente       ', Vista.buscar_cliente),
                        '3': ('    Modificar cliente    ', Vista.modificar_cliente),
                        '4': ('    Eliminar cliente     ', Vista.eliminar_cliente),
                        '0': ('    Volver atrás         ', Vista.menu_principal)}
        Vista.listar_clientes()
        Vista.imprimir_diccionario(dic_clientes)
        opcion = Vista.leer_opcion_menu(dic_clientes)   
        # se llama al metodo correspondiente a la opcion seleccionada
        dic_clientes[opcion][1]()

    # -------- Metodos relacionados a los clientes --------
    @staticmethod
    def agregar_cliente(ci=None, comprobante=False):
        """Metodo para agregar un nuevo cliente"""

        descripcion = 'NUEVO CLIENTE'
        Vista.listar_clientes()
        # se guardan los valores correspondientes a los datos del cliente 
        # si la cedula ya existe, se muestra un mensaje
        Vista.imprimir_titulo(descripcion)
        print('\n')
        Vista.imprimir_subtitulo_verde('Cedula')
        if ci != None:
            print(str(ci))
        else:
            ci = Vista.leer_numero()
        Vista.imprimir_subtitulo_verde('Nombre')
        nombre = Vista.leer_texto()
        Vista.imprimir_subtitulo_verde('Apellido')
        apellido = Vista.leer_texto()
        # se invoca al metodo del controlador para agregar cliente
        # se imprime un mensaje si se agregó el cliente o si ya existía 
        # uno con la misma cedula
        mensaje = Controlador.agregar_cliente(ci, nombre, apellido)
        Vista.limpiar_pantalla()
        Vista.imprimir_titulo(descripcion)
        Vista.listar_clientes()
        Vista.imprimir_centrado_verde(mensaje)
        # si no se agrega para comprobante de venta, 
        # se sigue con el flujo normal
        if not comprobante:
            Vista.pedir_enter()
            Vista.menu_clientes()

    @staticmethod
    def listar_clientes():
        """Metodo para listar los clientes"""

        # se imprime una lista con los datos de los clientes registrados
        Vista.mostrar_barra_menus(clientes=True)
        descripcion = 'LISTA DE CLIENTES'
        titulos = '{:8}'.format('Cedula') + \
                    '{:15}'.format('Nombre') + \
                    '{:15}'.format('Apellido')
        lista = Controlador.lista_clientes()
        Vista.imprimir_delimitador()
        Vista.imprimir_titulo(descripcion)
        Vista.imprimir_delimitador()
        Vista.imprimir_texto_rojo(titulos)
        Vista.imprimir_delimitador()
        for i in lista:
                print(' ', i)
    
    @staticmethod
    def buscar_cliente():
        """Metodo para buscar un cliente"""

        descripcion = 'BUSCAR CLIENTE'
        Vista.listar_clientes()
        Vista.imprimir_titulo(descripcion)
        print('\n')
        Vista.imprimir_subtitulo_verde('Cédula')
        ci = Vista.leer_numero()
        cliente = Controlador.existe_cliente(ci)
        # si no existe cliente con la cedula indicada,
        # se muestra un mensaje
        Vista.listar_clientes()
        Vista.imprimir_titulo(descripcion)
        if cliente == None:
            Vista.imprimir_centrado_verde('No existe el cliente con CI: ')
            print('\t', ci)
        else:
            Vista.imprimir_centrado_verde('Se encontró el cliente')
            print('\t', cliente)
        Vista.pedir_enter()
        Vista.menu_clientes()

    @staticmethod
    def modificar_cliente():
        """Método para modificar un cliente"""

        descripcion = descripcion = 'MODIFICAR CLIENTE'
        Vista.listar_clientes()
        Vista.imprimir_titulo(descripcion)
        print('\n')
        Vista.imprimir_subtitulo_verde('Cédula')
        ci = Vista.leer_numero()
        cliente = Controlador.existe_cliente(ci)
        Vista.listar_clientes()
        if cliente == None:
            Vista.imprimir_centrado_verde('No existe el cliente con CI:')
            print('\t', ci)
        else:
            Vista.imprimir_centrado_verde('Cliente:')
            print('\t', cliente)
            Vista.imprimir_subtitulo_verde('Nombre')
            nombre = Vista.leer_texto()
            Vista.imprimir_subtitulo_verde('Apellido')
            apellido = Vista.leer_texto()
            mensaje = Controlador.modificar_cliente(ci, nombre, apellido)
            Vista.listar_clientes()
            Vista.imprimir_centrado_verde(mensaje)
        Vista.pedir_enter()
        Vista.menu_clientes()

    @staticmethod
    def eliminar_cliente():
        """Método para eliminar un cliente"""

        Vista.listar_clientes()
        descripcion = 'ELIMINAR CLIENTE'
        Vista.imprimir_subtitulo_verde('Cédula')
        ci = Vista.leer_numero()
        cliente = Controlador.existe_cliente(ci)
        Vista.listar_clientes()
        if cliente == None:
            Vista.imprimir_centrado_verde('No existe el cliente con CI:')
            print('\t', ci)
        else:
            Vista.imprimir_centrado_verde('\n¿Seguro que desea eliminar el cliente? [S/N]')
            print('\t', cliente)
            opcion = Vista.leer_opcion_menu({'S': 'Sí',
                                            'N': 'No'}) 
            if opcion == 'S':
                mensaje = Controlador.eliminar_cliente(ci)
                Vista.listar_clientes()
                Vista.imprimir_centrado_verde(mensaje)
        Vista.pedir_enter()
        Vista.menu_clientes()
      
    # -------- Metodos relacionados a las ventas --------
    @staticmethod
    def boton_opcion_imprimir():
        """Metodo que crea un boton para imprimir 
            un comprobante o continuar con la carga 
            de productos
        """

        dic_botones = {'+': ('    Agregar articulo        ', Vista.menu_tipos_productos),
                       '-': ('    Eliminar articulo       ', Vista.eliminar_articulo),
                       '1': ('    Cancelar orden          ', Vista.cancelar_orden),
                       '0': ('    Imprimir comprobante    ', Vista.terminar_venta)}
        Vista.listar_productos_orden()
        Vista.imprimir_diccionario(dic_botones)
        opcion = Vista.leer_opcion_menu(dic_botones) 
        # se llama al metodo correspondiente a la opcion seleccionada
        dic_botones[opcion][1]()

    @staticmethod
    def eliminar_articulo():
        """Metodo para eliminar articulo de una orden"""
        Vista.listar_productos_orden()
        lista = Controlador.ver_producto_orden()
        if not lista:
            Vista.imprimir_centrado_verde('No hay articulos para eliminar')
        else:
            Vista.imprimir_subtitulo_verde('Ingrese una opcion')
            opcion = Vista.leer_numero()
            mensaje = Controlador.eliminar_articulo_orden(opcion)
            Vista.listar_productos_orden()
            Vista.imprimir_centrado_verde(mensaje)
        Vista.pedir_enter()
        Vista.boton_opcion_imprimir()

    @staticmethod
    def crear_orden():
        """Método para crear una nueva orden"""

        Controlador.crear_orden()
        Vista.boton_opcion_imprimir()

    @staticmethod
    def cancelar_orden():
        """Método para cancelar una orden activa"""

        Vista.listar_productos_orden()
        Vista.imprimir_centrado_verde('\n¿Cancelar oden? [S/N]')
        opcion = Vista.leer_opcion_menu({'S': 'Sí',
                                         'N': 'No'}) 
        if opcion.upper() == 'S':
            mensaje = Controlador.cancelar_orden()
            Vista.imprimir_centrado_rojo(mensaje)
            time.sleep(2)
            Vista.menu_productos()
        else:
            Vista.boton_opcion_imprimir()

    @staticmethod
    def terminar_venta():
        """Metodo para concluir con una venta e imprimir un comprobante"""

        descripcion = 'IMPRIMIR COMPROBANTE'    
        dic_comprobante = {'1': ('     Con nombre     ', None),
                           '2': ('     Sin nombre     ', None)}
        Vista.mostrar_barra_menus(productos=True)
        Vista.imprimir_titulo(descripcion)
        Vista.imprimir_diccionario(dic_comprobante)
        opcion = Vista.leer_opcion_menu(dic_comprobante)
        comprobante = None
        # si se requiere comprobante con nombre
        if opcion == '1':
            Vista.limpiar_pantalla()
            Vista.imprimir_titulo(descripcion)
            Vista.listar_clientes()
            print('\n')
            Vista.imprimir_subtitulo_verde('Cedula')
            ci = Vista.leer_numero()
            cliente = Controlador.existe_cliente(ci)
            # si no existe el cliente, se crea uno nuevo
            if cliente == None:
                Vista.imprimir_titulo(descripcion)
                Vista.agregar_cliente(ci, comprobante=True)
                cliente = Controlador.existe_cliente(ci)
            # de lo contrario, se guarda el cliente que existe con esta cedula
            else:
                Vista.imprimir_centrado_rojo('Cliente encontrado')
                print(cliente)
            comprobante = Controlador.obtener_comprobante(cliente)
            Vista.pedir_enter()
        else:
            # si se requiere comprobante si nombre, se pasa None como paramentro
            comprobante = Controlador.obtener_comprobante(None)
        Vista.limpiar_pantalla()
        # se imprime el comprobante
        Vista.imprimir_titulo(descripcion)
        print(Fore.YELLOW, comprobante)
        print(Style.RESET_ALL)
        Vista.pedir_enter()
        Vista.menu_principal()
    
    @staticmethod
    def listar_productos_orden():
        """Metodo para mostrar los articulos que se han cargado en la orden"""

        Vista.mostrar_barra_menus(productos=True)
        desc_titulo =    'LISTA DE PRODUCTOS'
        # se obitene la lista de productos en la orden
        lista = Controlador.ver_producto_orden()
        Vista.imprimir_titulo(desc_titulo)
        Vista.imprimir_delimitador()       
        if not lista:
            Vista.imprimir_centrado_verde('No hay articulos seleccionados')
        else:
            print('Nº  ','{:25}'.format('Descripcion'), '{:9}'.format('Precio'))
            Vista.imprimir_delimitador() 
            for i in lista:
                print(str(lista.index(i) + 1), '  ', i)
            Vista.imprimir_delimitador()  
        total = Controlador.total_orden()
        Vista.imprimir_centrado_verde('Total: Gs. ' + str(total))  
 
    # -------- Metodos adicionales del negocio
    @staticmethod
    def mostrar_total_caja():
        """Método para mostrar el total de capital en caja del día"""

        descripcion = 'TOTAL EN CAJA'
        Vista.limpiar_pantalla()
        Vista.imprimir_titulo(descripcion)
        Vista.imprimir_centrado_verde('Gs. ' + str(Controlador.total_caja()))
        print('\nFecha:\t', datetime.now().strftime('%d-%m-%Y'))
        print('Hora:\t', datetime.now().strftime('%H:%M'))
        Vista.pedir_enter()
        Vista.menu_principal()

    @staticmethod
    def realizar_cierre():
        """Método para realizar el cierre de caja"""
        
        Vista.imprimir_centrado_verde('\n¿Seguro que desea cerrar caja? [S/N]')
        opcion = Vista.leer_opcion_menu({'S': 'Sí',
                                            'N': 'No'}) 
        if opcion.upper() == 'S':
            Vista.limpiar_pantalla()
            dia = datetime.now().strftime('%d-%m-%Y')
            hora = datetime.now().strftime('%H:%M')
            resumen = Controlador.hacer_cierre(dia)
            Vista.imprimir_titulo('CIERRE DE CAJA')
            print('\n')
            Vista.imprimir_centrado_verde('Dia:  ' + dia)
            Vista.imprimir_centrado_verde('Hora: ' + hora)
            print('\n')
            Vista.imprimir_titulo('TOTAL RECAUDADO')
            print('\n')
            Vista.imprimir_centrado_verde('Gs. ' + str(Controlador.total_caja()))
            print('\n')
            Vista.imprimir_titulo('ORDENES DE VENTA DEL DIA')
            for i in resumen:
                print(i)
        else:
            Vista.menu_principal()

    @staticmethod
    def ver_cierres():
        """Metodo que muestra al usuario las ordenes cargadas en un dia determinado"""
        
        Vista.limpiar_pantalla()
        Vista.imprimir_titulo('CIERRES ANTERIORES')
        print('\n')
        Vista.imprimir_subtitulo_verde('Ingrese un día [dd-mm-AAAA]:')
        fecha = Vista.leer_fecha()
        ordenes = Controlador.ver_ordenes_dia(fecha)
        for i in ordenes:
            print(i)
        Vista.pedir_enter()
        Vista.menu_principal()

    