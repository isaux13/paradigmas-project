class Error(Exception):
   """Base class for other exceptions""" 

class CedulaMuyGrandeError(Error):
   """Raised when the input value is too large"""

class NumeroNegativoError(Error):
    """Raised when the input value is negative"""

class SinOrdenError(Error):
    """Raised when the order hasn't been created"""

class SinHeladeriaError(Error):
    """Raised when the heladeria hasn't been created"""
    
class OrdenVaciaError(Error):
    """Raised when the order hasn't been charged"""
    
class NumeroFlotanteError(Error):
    """Raised when the number is a float"""
    
class SinTextoError(Error):
    """Raised when the text is empty"""
    
class NoDevolvibleError(Error):
    """Raised when the product can not been returned"""
    
class SinOrdenesDiaError(Error):
    """Raised when there's no orders"""
    